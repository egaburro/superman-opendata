clc
clear all
close all

%%% 5

ICType     = 'TovStar'; %'TovStar';
WB         = 0;         %if =1 WB; if=0 NOWB; =2 mixed Graphics
ICType2    = 1;         %if =1 Perturbation; if=0 NoPerturbation
StartPoint = 'xL1-14-0cella';
FluxName   = 'Rusanov';
ifileend   = 6000;%1e6;
logGraph   = 0; %if =1 semilogy, if =0 plot  =2 plot all variables

if WB == 1
    if ICType2 == 1
        basefile = sprintf('%s-WB-Perturbation-%s-%s', ICType, StartPoint,FluxName);
    else
        basefile = sprintf('%s-WB-NOPerturbation-%s-%s', ICType, StartPoint,FluxName);
    end
elseif WB == 0
    if ICType2 == 1
        basefile = sprintf('%s-NOWB-Perturbation-%s-%s', ICType, StartPoint,FluxName);
    else
        basefile = sprintf('%s-NOWB-NOPerturbation-%s-%s', ICType, StartPoint,FluxName);
    end
elseif WB == 2
    if ICType2 == 1
        basefile = sprintf('%s-WB-Perturbation-%s-%s', ICType, StartPoint,FluxName);
        basefile1 = sprintf('%s-NOWB-Perturbation-%s-%s', ICType, StartPoint,FluxName);
    else
        basefile = sprintf('%s-WB-NOPerturbation-%s-%s', ICType, StartPoint,FluxName);
        basefile1 = sprintf('%s-NOWB-NOPerturbation-%s-%s', ICType, StartPoint,FluxName);
    end
end

% TOVStar-WB-Perturbation-xL01-Rusanov-00000000
% 

if logGraph ==1    
    fileNameAvi = 'CCZ4_Kerr_WBprova';
else
    fileNameAvi = 'CCZ4_Kerr_WBprova';
end
video = VideoWriter(fileNameAvi);
video.FrameRate = 2;

i = 0;
% open(video);
for ifile = 1:1:ifileend
    i = i+1;
    inputfile = sprintf('%s-%8.8i.dat', basefile, ifile');
    fid = fopen(inputfile,'r');
    if fid ~= -1        
        [IMAX, time, x, g11, g12, g13, g22, g23, g33, A11, A12, A13, A22, A23, A33, Theta, G1, G2, G3, lapse, shift1, shift2, shift3, b1, b2, b3, A1, A2, A3, B11, B21, B31, B12, B22, B32, B13, B23, B33, D111, D112, D113, D122, D123, D133, D211, D212, D213, D222, D223, D233, D311, D312, D313, D322, D323, D333, K, phi, P1, P2, P3, K0, rho, u, v, w, p, all]  = ReadFortranDataCCZ4(inputfile);
        if logGraph==1
            subplot(1,2,1)
            semilogy(x, rho)
            ylabel('rho')
            title(sprintf('CCZ4 WB from 0'));
            subplot(1,2,2)
            semilogy(x, p)
            ylabel('p')
            title(sprintf('Time = %f', time));
        elseif logGraph==0
            hold off
            subplot(2,2,1)
            plot(x, rho)
            ylabel('rho')
%             axis([0,14,-3e-7, 3e-7])
            title(sprintf('CCZ4 WB from 0'));
            subplot(2,2,2)
            plot(x, p)
            ylabel('p')
            title(sprintf('Time = %f', time));
            subplot(2,2,3)
            plot(x, u)
            ylabel('u')
            subplot(2,2,4)
            plot(x, K)
            ylabel('K')
        elseif logGraph==2
%             figure('units','normalized','outerposition',[0 0 1 1])
             for nVar = 1:64
                 hold off
                 subplot(8,8,nVar)
                 plot(x(:), all(nVar,:),'b')                 
             end  
              title(sprintf('Time = %f', time));
        end 
            drawnow           
    end
    if WB == 2
        inputfile = sprintf('%s-%8.8i.dat', basefile1, ifile');
        fid = fopen(inputfile,'r');
        if fid ~= -1
            [IMAX, time, x, g11, g12, g13, g22, g23, g33, A11, A12, A13, A22, A23, A33, Theta, G1, G2, G3, lapse, shift1, shift2, shift3, b1, b2, b3, A1, A2, A3, B11, B21, B31, B12, B22, B32, B13, B23, B33, D111, D112, D113, D122, D123, D133, D211, D212, D213, D222, D223, D233, D311, D312, D313, D322, D323, D333, K, phi, P1, P2, P3, K0, rho, u, v, w, p, all]  = ReadFortranDataCCZ4(inputfile);
            if logGraph==2
                %             figure('units','normalized','outerposition',[0 0 1 1])
                for nVar = 1:64
                    subplot(8,8,nVar)
                    hold on
                    plot(x(:), all(nVar,:),'r')
                    hold off
                end
                title(sprintf('Time = %f', time));
            elseif logGraph==0
                subplot(2,2,1)
                hold on
                plot(x, Theta,'r')
                hold off
                ylabel('theta')
                title(sprintf('CCZ4 WB from 0'));
                subplot(2,2,2)
                hold on
                plot(x, D122,'r')
                hold off
                ylabel('D122')
                title(sprintf('Time = %f', time));
                subplot(2,2,3)
                hold on
                plot(x, D133,'r')
                ylabel('D133')
                hold off
                subplot(2,2,4)
                hold on
                plot(x, K,'r')
                hold off
                ylabel('K')
            end
            drawnow 
            
%             F(i) = getframe(gcf) ;
%             writeVideo(video, F(i))
%             pause(1)
%             fid=fclose('all');
%             
            fid=fclose('all');
        end
    end
    fid=fclose('all');
end
% close(video)
