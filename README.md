# SuPerMan-OpenData

This folder contains part of the numerical results produced by the SuPerMan project (Horizon 2020, MSCA-IF, n. 101025563).
The data are stored here only for a temporary period, generally between submission and publication of the corresponding paper, 
or until we judge they could be useful for future comparison.		


*** FV2WB-GRMHD&CCZ4 ***

This folder contain:
- The preprint of the paper describing the framework, the novelty and the results of the work contained in this folder
- The tecplot layout file of the images contained in the paper (to facilitate the comparison with usual collaborators using the same post-data processing)
- Some brut data as produced by our code: they are available for all interested people, but not documentation is given
